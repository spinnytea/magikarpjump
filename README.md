Magikarp Jump
=============

Simply put, buying food is tricky. Sure, the beginning is easy, just level up the two that you have as quickly as you can. But once you max out the first two foods...


Pro strats
----------

It's best to wait to unlock food only when it is on part with the food you've already unlocked (i.e. don't unlock _Pecha Berry_ until you can max it out, don't unlock _Lava Cookie_ until you can take it straight to level 95.)

This is just a simple cheatsheet of when to unlock them. [all.txt](./all.txt) is the complete output. You can run `npm start -- -h` to see some options for narrowing in on where you are.


Bonus
-----

Training is fun to level up. But at first money is tight and training isn't _necessary_ to level up. I mean, it's fun to open them all up right away just so you can have some variation, and the higher level ones are definitely beneficial.

Training is useful to unlock more candy, which you get once every 5 levels. So there is a breakdown for the next cheapest training candy.


References
-----

https://www.pokemon.com/us/app/pokemon-magikarp-jump

https://play.google.com/store/apps/details?id=jp.pokemon.koiking

