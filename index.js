const fs = require('fs');
const _ = require('lodash');
const cheerio = require('cheerio');
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')

// doFood(3); // of 17
// doTraining(40); // of 85

yargs(hideBin(process.argv))
	.command('both [food] [training]', 'print both tables', (yargs) => {
		yargs
			.positional('food', {
				type: 'number',
				describe: 'how many foods you have maxed',
				default: 0,
				demand: true,
			})
			.positional('training', {
				type: 'number',
				describe: 'how far you have leveled up training',
				default: 0,
				demand: true,
			})
	}, (argv) => {
		doFood(argv.food, argv.size)
		doTraining(argv.training, argv.size)
	})
	.command('food [progress]', 'only print the food table', (yargs) => {
		yargs
			.positional('progress', {
				type: 'number',
				describe: 'how many foods you have maxed',
				default: 0,
			})
	}, (argv) => {
		doFood(argv.progress, argv.size)
	})
	.command('training [progress]', 'only print the training table', (yargs) => {
		yargs
			.positional('progress', {
				type: 'number',
				describe: 'how many foods you have maxed',
				default: 0,
			})
	}, (argv) => {
		doTraining(argv.progress, argv.size)
	})
	.option('size', {
		alias: 's',
		type: 'number',
		description: 'How many rows to print',
		default: 5,
	})
	.alias('help', 'h')
	.example([
		['$0 both 3 40', 'print both tables based on progress'],
		['$0 food 3', 'print the next few food unlocks'],
		['$0 training 40', 'print the next few training unlocks'],
		['$0 food 0 -s 99', 'print the whole food table'],
		['$0 training 0 -s 99', 'print the whole training table'],
	])
	.argv;

function doFood(progress, count) {
	fs.readFile('./data/Pokémon_ Magikarp Jump - Food.html', 'utf8', function (err, data) {
		if (err) throw err;
		const $ = cheerio.load(data);
		let foods = getTables($).map(function(e) {
			return {
				name: parseName($, e),
				data: parseTable($, e),
			};
		});
		console.log(`:: Food (${foods.length})`);
		_.chain(foods)
			.forEach(function(food) {
				food.level = 0;
				food.cost = 0;
				var firstLarger = true;
				food.data.forEach(function(o) {
					if(o.power <= 3000000) {
						food.level = o.level;
						food.cost += o.cost;
					} else if(firstLarger) {
						firstLarger = false;
						food.level = o.level;
						food.cost += o.cost;
					}
				});
			})
			.map((o) => _.pick(o, ['name', 'level', 'cost']))
			// .map((o, i) => _.assign({ progress: i+1 }, o))
			.slice(progress, progress + count)
			.thru(console.log)
			.value();
	});
}

function doTraining(progress, count) {
	fs.readFile('./data/Pokémon_ Magikarp Jump - Training.html', 'utf8', function (err, data) {
		if (err) throw err;
		const $ = cheerio.load(data);
		let training = getTables($).map(function(e) {
			return {
				name: parseName($, e),
				data: summarizeTable(parseTable($, e)),
			};
		});

		const trainingCount = _.size(training) * _.size(training[0].data);
		console.log(`:: Training (${trainingCount})`);
		_.chain(training)
			.map(function(t) {
				return _.map(t.data, function(cost, range) { return { name: t.name, range: range, cost: cost }; });
			})
			.flatten()
			// unlock all before listing ranges
			.orderBy([({range}) => (range === '1'), 'cost'], ['desc', 'asc'])
			.map((o, i) => [i+1, _.padEnd(o.name, 18), _.padStart(o.range, 6), o.cost])
			.slice(progress, progress + count)
			.thru(console.log)
			.value();
	});
}

function getTables($) {
	const tables = $('#moves table')
		.filter((idx, elem) => $(elem).find('>tbody>tr').length === 101)
		.toArray();
	// for some reason, they are doubled
	if(tables.length !== 34) throw new Error('incorrect table count');
	return tables.slice(0, 17);
}
function parseName($, listElem) {
	const nameRow = $(listElem).parents('table.tab').find('>tbody').children()[1];
	const nameCell = $(nameRow).children()[1];
	return $(nameCell).text();
}
function parseTable($, listElem) {
	return $(listElem).find('tr').map(function(idx, elem) {
		if(idx === 0) return null;
		const cells = $(elem).children();
		return {
			level: parseNumber($(cells[0]).text()),
			power: parseNumber($(cells[1]).text()),
			cost: parseNumber($(cells[2]).text()),
		};
	}).toArray().slice(1);
}
function parseNumber(str) {
	let num = str = str.trim();
	switch(num[num.length-1]) {
		case 'K':
		case 'k':
			num = +(parseFloat(num) * 1000);
			break;
		case 'M':
			num = +(parseFloat(num) * 1000000);
			break;
		default:
			num = +(num.replace(/,/g, ''));
			break;
	}
	// error checking
	if(num !== +num) console.log('error', str);
	return num;
}

function summarizeTable(table) {
	const summary = {
		'1': 0,
		'2-25': 0,
		'26-50': 0,
		'51-75': 0,
		'76-100': 0,
	};
	table.forEach(function(o, idx) {
		if(idx === 1) summary['1'] += o.cost;
		else if(idx < 26) summary['2-25'] += o.cost;
		else if(idx < 51) summary['26-50'] += o.cost;
		else if(idx < 76) summary['51-75'] += o.cost;
		else summary['76-100'] += o.cost;
	});
	return summary;
}
